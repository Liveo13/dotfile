if status is-interactive
    # Commands to run in interactive sessions can go here
end

### "bat" as manpager 
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

### AUTOCOMPLETE AND HIGHLIGHT COLORS ### 
set fish_color_normal brcyan 
set fish_color_autosuggestion '#7d7d7d' 
set fish_color_command brcyan 
set fish_color_error '#ff6c6b' 
set fish_color_param brcyan 

## Aliases
### Basic program replacements

alias grep="ripgrep"
alias cat="bat"
alias ls="exa"
alias ps="procs"

# navigation 
alias ..='cd ..' 
alias ...='cd ../..' 
alias .3='cd ../../..' 
alias .4='cd ../../../..' 
alias .5='cd ../../../../..' 

# Changing "ls" to "exa" 
alias ls='exa -al --color=always --group-directories-first' # my preferred listing 
alias la='exa -a --color=always --group-directories-first'  # all files and dirs 
alias ll='exa -l --color=always --group-directories-first'  # long format 
alias lt='exa -aT --color=always --group-directories-first' # tree listing 
alias l.='exa -a | egrep "^\."'

# adding flags 
alias df='df -h'                          # human-readable sizes 
alias free='free -m'                      # show sizes in MB 
alias lynx='lynx -cfg=~/.lynx/lynx.cfg -lss=~/.lynx/lynx.lss -vikeys' 
alias vifm='./.config/vifm/scripts/vifmrun' 
alias ncmpcpp='ncmpcpp ncmpcpp_directory=$HOME/.config/ncmpcpp/' 
alias mocp='mocp -M "$XDG_CONFIG_HOME"/moc -O MOCDir="$XDG_CONFIG_HOME"/moc'

# ps 
alias psa="ps auxf" 
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e" 
alias psmem='ps auxf | sort -nr -k 4' 
alias pscpu='ps auxf | sort -nr -k 3'

# switch between shells 
# I do not recommend switching default SHELL from bash. 
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'" 
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'" 
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"
