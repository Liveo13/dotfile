# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Use powerline - Powerline is a statusline plugin for vim, and provides statuslines and 
# prompts for several other applications, including zsh, bash, tmux, IPython, Awesome, i3 and Qtile.
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=3000
SAVEHIST=3000
setopt notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/liveo13/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Add Anaconda3 to path
export PATH=$HOME/anaconda3/bin/:$PATH

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin/:$PATH

# Add my own scripts to path
export PATH=$HOME/liveo13/MEGA/4.Scripts:$PATH

# Add Cargo (Rust)
export PATH=$HOME/.cargo/bin:$PATH

# Add dmenu stuff to path
export PATH=$HOME/.dmenu:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Allow settings in i3
export XDG_CURRENT_DESKTOP=GNOME

# Disable the C shell-style command history, which treats ‘!’ as a special character.
set +H

# vi mode
#set -o vi
bindkey -v

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="jonathan"
ZSH_THEME="3den"
#ZSH_THEME="powerlevel10k/powerlevel10k"

# Stuff for zsh-z which can be found here:
# https://github.com/agkozak/zsh-z
#source ~/.config/zsh/zsh-z.plugin.zsh
###autoload -U compinit && compinit
#zstyle ':completion:*' menu select

# Include z - https://www.linuxuprising.com/2019/02/zlua-faster-way-of-changing-directories.html
#eval "$(lua /home/liveo13/z.lua/z.lua --init zsh)"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
#ZSH_THEME_RANDOM_CANDIDATES=( "dogenpunk" "agnoster" )


# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=7
# Uncomment the following line to change how often to auto-update (in days).
zstyle ':omz:update' frequency 13
zstyle ':omz:update' mode auto      # update automatically without asking

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

export EDITOR=vim

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  colored-man-pages
  #zsh-autosuggestions
  sudo
  copypath
  copyfile
  dirhistory
  history
  web-search
  copybuffer
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# plugins=(git colored-man-pages)
# plugins=(git zsh-syntax-highlighting)

# ****** Add to path ********

# Scala Activator
export PATH="$PATH:/opt/activator"

# Ghostery
export PATH=/opt/Ghostery:$PATH

xdg-mime default org.pwmt.zathura.desktop application/pdf

# export PIG_HOME=/home/edureka/pig-0.16.0
# export PATH=$PATH:/h>ome/edureka/pig-0.16.0/bin
# export PIG_CLASSPATH=$HADOOP_CONF_DIR
# ****** End to path *********

# Swap eac and CAPS
# setxkbmap -layout us -option ctrl:nocaps
# Set Ultisnips default folder
# let g:UltiSnipsSnippetsDir = "~/.vim/bundle/ultisnips/UltiSnips"

# Set grep to color
export GREP_OPTIONS='--color=auto'

# Go variables
export GOROOT=$HOME/go
export PATH=$PATH:$GOROOT/bin

export GOPATH=$HOME/golib
export PATH=$PATH:$GOPATH/bin

# Common commands
alias cs='clear;ls'
alias mk='mkdir'
alias la='ls -a --color=auto'
alias ll='ls -l --color=auto'
alias l='ls -lah --color=auto'
export GREP_COLOR='1;37;41'
alias grep='grep --color=auto'
alias cm='chmod +x'
alias less='most'
alias open='xdg-open'
alias go='gotop' #Recplaces htop
alias ..='cd ..'
alias ...='cd ..; c>d ..'
alias ....='cd ..; cd ..; cd ..'
alias .....='cd ..; cd ..; cd ..; cd ..'

# git
alias g='git'
alias gst='git status'
alias gco='git commit -m'
alias gcl='git clone'
alias gal='git add .'
alias gad='git addi'
alias gps='git push -u origin master'
alias gpl='git pull'
alias ggr='git log --all --decorate --oneline --graph'

# Clipboard
alias clc='cb copy'
alias clt='cb cut'
alias clp='cb paste'

# Other
alias sa='source venv/bin/activate'
alias gsm='gnome system monitor'
alias myip='curl ipinfo.io/ip'

# Basic single letter command renames
alias b='sudo bandwhich'
alias c='cd'
alias d='dmenu_run'
alias h='history'
alias k='kill'
alias o='xdg-open'
alias p='pwd'
alias q='exit'
alias t='time'
alias u='ncdu'
alias v='vifm'

# Install
alias spi='sudo pip3 install'
alias spi2='sudo pip2 install'
alias soz='source $HOME/.zshrc'
alias vimrc='vim $HOME/.vimrc'
alias zshrc='vim $HOME/.zshrc'
alias alacrc='vim $HOME/.config/alacritty/alacritty.yml'
alias fli='sudo flatpak install'
alias flu='sudo flatpak uninstall'
alias flr='flatpak run'
alias fll='flatpak list'
alias paci='sudo pacman -S'
alias pacu='sudo pacman -Syu'
alias pacs='sudo pacman -Ss'
alias pacr='sudo pacman -R'
alias fixpaclock="sudo rm /var/lib/pacman/db.lck"
alias fixpacdb="sudo pacman -SSyyu"
alias fli='sudo flatpak install flathub'
alias sai='sudo apt install'
alias sar='sudo apt remove'
alias sas='sudo apt-get search'
alias sau='sudo apt update && sudo apt upgrade'
alias y='yay -S'
  
# Virtualenv
alias venvy='python3 -m virualenv venv'
alias svenv='source venv/bin/activate'

# Expressvpn
alias evc='expressvpn connect'
alias evd='expressvpn disconnect'
alias evs='expressvpn status'
alias ev='expressvpn'
alias evl='expressvpn preferences set network_lock off'

# cd locations
alias rt='cd /'
alias desk='cd ~/Desktop'
alias down='cd ~/Downloads'
alias docs='cd ~/Documents'
alias proj='cd "$HOME/MEGA/2. Projects/1. Python"'
alias tmp='cd ~/MEGA/temp'
alias con='cd ~/.config'

# Interaction
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'
alias ln='ln -i'

# Documents
alias tt='zathura /home/liveo13/MEGA/1.\ School/1.\ General/timetable.pdf'

# Lynx
alias ly='lynx -accept_all_cookies -cfg="~/.config/lynx/lynx.cfg" -lss="~/.config/lynx/lynx.lss"'
# alias lynx='lynx -accept_all_cookies'
alias lcfg='~/.config/lynx/lynx.cfg'

export _lynx=$(which lynx)

#lynxx() {
  #if [[ -z "$_lynx" ]]; then
    #echo "Lynx ain't installed!"
    #return 1
  #fi
  #[[ -r ~/.config/lynx/lynx.cfg ]] && lynxcfg="-cfg=$HOME/.config/lynx/lynx.cfg";
  #[[ -r ~/.config/lynx/lynx.lss ]] && lynxlss="-cfg=$HOME/.config/lynx/lynx.lss";
  #$_lynx $lynxcfg $lynxcss "$*"
#} && export -f lynxx

# tmux
alias tms='tmux source-file ~/.tmux.conf'

## To remember windows session, logoff using: systemctl suspend
alias lo='systemctl suspend'

## Weather
alias weat='curl wttr.in'

## Rofi
alias rs='pkill sxhkd; sxhkd -m 1&'

# Making directories
alias m='mkdir -pv'
alias mkdir='mkdir -pv'
function mcd () { mkdir -p "$@" && eval cd "\"\$$#\""; }

# i3 stuff
alias i3c='vim ~/.config/i3/config'

# Awesome stuff
alias awe='vim ~/.config/awesome/rc.lua'

# Qtile Stuff
alias qtl='vim ~/.config/qtile/config.py'

# taskwarrior
alias tl='task list'
alias ta='task add'
td ()
{
  task "$1" delete
}

# Other programs
alias grep="ripgrep"
alias cat="bat"
alias ls="exa"
alias ps="procs"

# Changing ls to exa
alias ls='exa -al --color=always --group-directories-first' #my preferred listing
alias la='exa -a --color=always --group-directories-first' #my preferred listing
alias ll='exa -l --color=always --group-directories-first' #my preferred listing
alias lt='exa -aT --color=always --group-directories-first' #my preferred listing
alias l.='exa -a | egrep "^\."'

# Adding flags
alias df='df -h'
alias free='free -m'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Confirm before closing
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

# systemctl - Jusat add the service name to the end
alias sysss='systemctl status'
alias sysst='systemctl start'
alias syssp='systemctl stop'
alias sysre='systemctl restart'
alias syski='systemctl kill'
alias sysen='systemctl enable'
alias sysid='systemctl disable'
alias sysse='systemctl --type=service --state=running'

# sxhkd
alias sxh='vim ~/.config/sxhkd/sxhkdrc'

# espanso
alias esp='vim ~/.config/espanso/default.yml'

# tmux
alias tmns='tmux new -s sess'
alias tmn='tmux new -s'

# Run my dotfile rofi script:
alias dot='~/scripts/rofi/rofi-dots.sh'

# Temporary

source /home/liveo13/.config/broot/launcher/bash/br

# Stuff for Garuda - To Be Sorted
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias psmem='ps auxf | sort -nr -k 4'
alias rmpkg="sudo pacman -Rdd"
alias tarnow='tar -acf '
alias untar='tar -zxvf '
alias upd='/usr/bin/update'
alias vdir='vdir --color=auto'
alias wget='wget -c '

# Get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Stuff from DT
# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

colorscript random
